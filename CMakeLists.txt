include_directories (headers)

set (SOURCES
     src/algebra_common.c
     src/algebra.c
     src/filter.c
     src/kalman.c
     src/matrix.c
     src/other.c
     src/polynomial.c
     src/quaternion.c
     src/ring_buffer.c
     src/vector.c)

set (HEADERS
     headers/rc/math/algebra.h
     headers/rc/math/filter.h
     headers/rc/math/kalman.h
     headers/rc/math/matrix.h
     headers/rc/math/polynomial.h
     headers/rc/math/quaternion.h
     headers/rc/math/ring_buffer.h
     headers/rc/math/vector.h
     headers/rc/math.h)

add_library (robot_control_math
             ${SOURCES}
             ${HEADERS})

